﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class Member
    {
        public String mebCode{get;set;}

        public String mebName { get; set; }

        public String mebCertNo { get; set; }

        public String birthDay { get; set; }

        public String phoneNo { get; set; }

        public String address { get; set; }

        public String cardNo { get; set; }

        public Decimal points { get; set; }

        public Decimal amount { get; set; }

        public String cardNum { get; set; }
    }
}
