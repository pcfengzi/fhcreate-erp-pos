﻿using System;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class PayType
    {
        
        public Int64 id {get;set;}

        public string payTypeKey {get;set;}

        public string payTypeName {get;set;}

        public string isEnable {get;set;}

        public string isLessCent {get;set;}

        public string isOverFlow { get; set; }

        public string isSubType { get; set; }

        public string enableReturn { get; set; }

        public string serviceKey { get; set; }

        public Int64 seq {get;set;}

        public Int64 ver { get; set; }

        public string openBox { get; set; }

    }
}
