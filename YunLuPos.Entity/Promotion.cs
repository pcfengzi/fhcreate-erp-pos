﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class Promotion
    {
        public Int64 id { get; set; }

        public string cliqueCode { get; set; }

        public string branchCode { get; set; }

        public string promCode { get; set; }

        public string barCode { get; set; }

        public string goodsCode { get; set; }

        public Double promPrice { get; set; }

        public string iStatus { get; set; }

        public string startDate { get; set; }

        public string endDate { get; set; }

        public string startTime { get; set; }

        public string endTime { get; set; }

        public String ver { get; set; }
    }
}
