﻿using System;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class SaleOrderGoods
    {
        
        public Int64 id {get;set;}

        public string cliqueCode {get;set;}

        public string orderId {get;set;}

        public string orderCode {get;set;}

        public string branchCode {get;set;}

        public string goodsCode {get;set;}

        public string barCode {get;set;}

        public string goodsName {get;set;}

        public string specs {get;set;}

        public string unit {get;set;}

        public Int64 packCount {get;set;}

        public Double price {get;set;}

        public Double disPrice {get;set;}

        public Double goodsCount {get;set;}

        public Double disCount {get;set;}

        public Double realAmount {get;set;}

        public Double payAmount {get;set;}

        public Double disAmount {get;set;}

        public string priceSource { get; set; }

        public string promCode { get; set; }

        public string autherCode { get; set; }

        public string autherName { get; set; }

    }
}
