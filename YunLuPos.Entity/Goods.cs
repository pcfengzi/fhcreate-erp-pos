﻿using System;
using System.Linq;
using System.Text;

namespace YunLuPos.Entity
{
    public class Goods
    {
        
        public Int64 id {get;set;}

        public string cliqueCode {get;set;}

        public string branchCode {get;set;}

        public string goodsCode {get;set;}

        public string barCode {get;set;}

        public string barCodeType { get; set; }

        public string goodsName {get;set;}

        public string shortName {get;set;}

        public string specs {get;set;}

        public string unitName {get;set;}

        public string catalogCode { get; set; }

        public string operateType { get; set; }

        public string saleType { get; set; }

        public string goodsType { get; set; }

        public string posPriceEnable { get; set; }

        public String barCodeCaption { get; set; }

        public Int64 unitCount { get; set; }

        public Double salePrice { get;set;}

        public Double vipPrice { get; set; }

        public string status { get;set;}

        public string dotCount { get; set; }

        public String ver { get; set; }

        public String deleteBarCode { get; set; }

    }
}
