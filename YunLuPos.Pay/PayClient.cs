﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Pay
{
    public class PayClient
    {
        private PayClient()
        {

        }

        static String separator = "/";
        static String basePath = Environment.CurrentDirectory + "/YunLuPay";

        public static Dictionary<String, Object> send(Dictionary<String, Object> map, String path)
        {
            if (path != null)
            {
                basePath = path;
            }
            Dictionary<String, Object> result = null;
            // 加载配置信息
            result = initConfig();
            if (result != null)
            {
                Logger.debug(result.ToString());
                return result;
            }
            // 填充基础配置信息
            fillBase(map);
            // 检查参数信息
            result = checkParam(map);
            if (result != null)
            {
                Logger.debug(result.ToString());
                return result;
            }
            // 执行请求
            result = HttpUtil.request(map, getBusinessType(map));
            return result;
        }

        /**
         * 	获取业务操作类型
         * @param map
         * @return
         */
        static String getBusinessType(Dictionary<String, Object> map)
        {
            String type = map["BUSINESS_TYPE"].ToString();
            if ("1".Equals(type))
            {
                return C.SERVER_PAY;
            }
            if ("2".Equals(type))
            {
                return C.SERVER_QUERY;
            }
            if ("3".Equals(type))
            {
                return C.SERVER_REFUND;
            }
            if ("4".Equals(type))
            {
                return C.SERVER_REFUND_QUERY;
            }
            if ("5".Equals(type))
            {
                return C.SERVER_CANCEL;
            }
            return "";
        }

        static void fillBase(Dictionary<String, Object> paraMap)
        {
            if (paraMap == null)
            {
                return;
            }
            if (!paraMap.ContainsKey("MCH_ID") || Tools.isEmpty(paraMap["MCH_ID"]))
            {
                paraMap["MCH_ID"] = Configs.MCH_ID;
            }
            if (!paraMap.ContainsKey("SHOP_CODE") || Tools.isEmpty(paraMap["SHOP_CODE"]))
            {
                paraMap["SHOP_CODE"] = Configs.SHOP_CODE;
            }
            if (!paraMap.ContainsKey("DEVICE_INFO") || Tools.isEmpty(paraMap["DEVICE_INFO"]))
            {
                paraMap["DEVICE_INFO"] = Configs.DEVICE_INFO;
            }
            if (!paraMap.ContainsKey("OPERATOR_ID") || Tools.isEmpty(paraMap["OPERATOR_ID"]))
            {
                paraMap["OPERATOR_ID"] = Configs.OPERATOR_ID;
            }
            if (!paraMap.ContainsKey("BODY") || Tools.isEmpty(paraMap["BODY"]))
            {
                paraMap["BODY"]=Configs.BODY;
            }
        }

        /**
         * 	 检查毕传参数是否传入
         * @param p
         * @return
         */
        static Dictionary<String, Object> checkParam(Dictionary<String, Object> p)
        {
            String s = "必要参数";
            Dictionary<String, Object> er = new Dictionary<String, Object>();
            er.Add(C.RESULT_CODE, C.FAIL);
            er.Add(C.FAIL_CODE, "MISS_PARAM");
            if (!p.ContainsKey("MCH_ID") || Tools.isEmpty(p["MCH_ID"]))
            {
                er.Add(C.RESULT_MSG, s + "MCH_ID");
                return er;
            }
            if (!p.ContainsKey("SHOP_CODE") || Tools.isEmpty(p["SHOP_CODE"]))
            {
                er.Add(C.RESULT_MSG, s + "SHOP_CODE");
                return er;
            }
            if (!p.ContainsKey("DEVICE_INFO") || Tools.isEmpty(p["DEVICE_INFO"]))
            {
                er.Add(C.RESULT_MSG, s + "DEVICE_INFO");
                return er;
            }
            if (!p.ContainsKey("OPERATOR_ID") || Tools.isEmpty(p["OPERATOR_ID"]))
            {
                er.Add(C.RESULT_MSG, s + "OPERATOR_ID");
                return er;
            }
            if (!p.ContainsKey("BUSINESS_TYPE") || Tools.isEmpty(p["BUSINESS_TYPE"]))
            {
                er.Add(C.RESULT_MSG, s + "BUSINESS_TYPE");
                return er;
            }
            if (!p.ContainsKey("OUT_TRADE_NO") || Tools.isEmpty(p["OUT_TRADE_NO"]))
            {
                er.Add(C.RESULT_MSG, s + "OUT_TRADE_NO");
                return er;
            }
            if (!p.ContainsKey("TOTAL_FEE") || Tools.isEmpty(p["TOTAL_FEE"]))
            {
                er.Add(C.RESULT_MSG, s + "TOTAL_FEE");
                return er;
            }
            else if (!Tools.isEmpty(p["TOTAL_FEE"]))
            {
                //处理支付金额
                try
                {
                    Double d = Double.Parse(p["TOTAL_FEE"].ToString());
                    p.Remove("TOTAL_FEE");
                    p.Add("TOTAL_FEE",(Int32)d);
                }
                catch
                {
                    er.Add(C.RESULT_MSG, s + "TOTAL_FEE");
                    return er;
                }
            }
            if (!p.ContainsKey("AUTH_CODE") || Tools.isEmpty(p["AUTH_CODE"]))
            {
                er.Add(C.RESULT_MSG, s + "AUTH_CODE");
                return er;
            }
            return null;
        }

        static Dictionary<String, Object> initConfig()
        {
            if (!Configs.isLoaded)
            {
                try
                {
                    Logger.impl = new FileLoggerImpl(basePath + separator + "PayLog.log");
                    Configs.initConfig(basePath + separator + "PayConfig.txt");
                }
                catch (ClientException e)
                {
                    return e.toMap();
                }
            }
            return null;
        }
    }
}
