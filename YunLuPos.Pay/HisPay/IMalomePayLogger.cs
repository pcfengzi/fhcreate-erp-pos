﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HisPay.Payment
{
    interface IPayLogger
    {
        Boolean isDebugEnable();

        void debug(String log);
    }
}
