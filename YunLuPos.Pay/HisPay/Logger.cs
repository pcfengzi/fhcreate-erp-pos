﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HisPay.Payment
{
    class Logger
    {
        public static IPayLogger impl = null;

        public static void debug(String log)
        {
            if (impl == null || !impl.isDebugEnable())
            {
                return;
            }
            impl.debug(log);
        }

        public static void debug(Exception e)
        {
            
        }
      
    }
}
