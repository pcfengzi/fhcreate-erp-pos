﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace PosSetting
{
    public class InIUtil
    {
        public static string ConfigPath = Environment.CurrentDirectory + "\\CONFIG.INI";
        public static string GenSection       = "GEMERAL";          //基础配置

        public static string PrintSection     = "PRINT";            //打印配置
        public static string PrintOption      = "PRINT_OPTION";     //打印类型
        public static string PrintComPort     = "COM_PORT";         //COM端口
        public static string PrintComRate     = "COM_RATE";         //COM波特率
        public static string PrintComBit      = "COM_BIT";          //COM数据位
        public static string PrintComStopBit  = "COM_STOPBIT";      //COM停止位
        public static string PrintComCheckBit = "COM_CHECKBIT";     //COM校验位
        public static string LptProt          = "LPT_PORT";         //并口端口
        public static string PrintFile        = "PRINT_FILE";       //打印文件名称

        public static string DisplaySection = "DISPLAY";            //客显
        public static string DisplayComPort = "D_COM_PORT";         //COM端口
        public static string DisplayComRate = "D_COM_RATE";         //COM波特率
        public static string DisplayComBit = "D_COM_BIT";          //COM数据位
        public static string DisplayComStopBit = "D_COM_STOPBIT";      //COM停止位
        public static string DisplayComCheckBit = "D_COM_CHECKBIT";     //COM校验位

        public static string BalanceSection = "BALANCE";            //称重码
        public static string BalanceStart = "B_START";         //COM端口
        public static string BalanceEnd = "B_END";         //COM波特率
        public static string Balance13 = "B_13";          //COM数据位
        public static string Balance18 = "B_18";      //COM停止位

        /// <summary>
        /// 写入基础配置
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool WriteGenData(string key,string value)
        {
            return WriteInIData(GenSection, key, value, ConfigPath);
        }

        /// <summary>
        /// 写入配置
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="path"></param>
        /// <returns></returns>
       public static bool WriteInIData(string section, string key, string value)
        {
            return WriteInIData(section, key, value, ConfigPath);
        }

        /// <summary>
        /// 写入配置
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        static bool WriteInIData(string section, string key, string value, string path)
        {
            if (File.Exists(path))
            {
                return WritePrivateProfileString(section, key, value, path);
            }
            else
            {
                return false;
            }
        }




        /// <summary>
        /// 读取基础配置
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string ReadGenData(string key)
        {
            return ReadInIData(GenSection, key, "", ConfigPath);
        }

        /// <summary>
        /// 根据配置文件读取配置
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="noText">默认值</param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string ReadInIData(string section, string key)
        {
            return ReadInIData(section, key, "", ConfigPath);
        }

        /// <summary>
        /// 根据配置文件读取配置
        /// </summary>
        /// <param name="section"></param>
        /// <param name="key"></param>
        /// <param name="noText">默认值</param>
        /// <param name="path"></param>
        /// <returns></returns>
        static string ReadInIData(string section,string key,string noText,string path)
        {
            if (File.Exists(path))
            {
                StringBuilder temp = new StringBuilder(1024);
                GetPrivateProfileString(section, key, noText, temp,1024, path);
                return temp.ToString();
            }else
            {
                return string.Empty;
            }
        }

        [DllImport("kernel32")]
        private static extern bool WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

    }
}
