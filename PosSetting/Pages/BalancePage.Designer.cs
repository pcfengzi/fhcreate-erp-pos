﻿namespace PosSetting.Pages
{
    partial class BalancePage
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BalancePage));
            this.label5 = new System.Windows.Forms.Label();
            this.bEndWith = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.bCode18Combox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.bCode13Combox = new System.Windows.Forms.ComboBox();
            this.bDeptTextbox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(77, 14);
            this.label5.TabIndex = 29;
            this.label5.Text = "标签码结束";
            // 
            // bEndWith
            // 
            this.bEndWith.Location = new System.Drawing.Point(137, 43);
            this.bEndWith.Name = "bEndWith";
            this.bEndWith.Size = new System.Drawing.Size(176, 23);
            this.bEndWith.TabIndex = 28;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(105, 14);
            this.label4.TabIndex = 27;
            this.label4.Text = "标签类型(18位)";
            // 
            // bCode18Combox
            // 
            this.bCode18Combox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bCode18Combox.FormattingEnabled = true;
            this.bCode18Combox.Items.AddRange(new object[] {
            "DDFFFFFWWWWWEEEEEC",
            "DDFFFFFEEEEEWWWWWC"});
            this.bCode18Combox.Location = new System.Drawing.Point(137, 118);
            this.bCode18Combox.Name = "bCode18Combox";
            this.bCode18Combox.Size = new System.Drawing.Size(176, 22);
            this.bCode18Combox.TabIndex = 26;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 14);
            this.label3.TabIndex = 24;
            this.label3.Text = "标签类型(13位)";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(12, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(525, 182);
            this.label2.TabIndex = 23;
            this.label2.Text = resources.GetString("label2.Text");
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 14);
            this.label1.TabIndex = 22;
            this.label1.Text = "标签码开头";
            // 
            // bCode13Combox
            // 
            this.bCode13Combox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bCode13Combox.FormattingEnabled = true;
            this.bCode13Combox.Items.AddRange(new object[] {
            "DDFFFFFEEEEEC",
            "DDFFFFFWWWWWC"});
            this.bCode13Combox.Location = new System.Drawing.Point(137, 80);
            this.bCode13Combox.Name = "bCode13Combox";
            this.bCode13Combox.Size = new System.Drawing.Size(176, 22);
            this.bCode13Combox.TabIndex = 21;
            // 
            // bDeptTextbox
            // 
            this.bDeptTextbox.Location = new System.Drawing.Point(137, 7);
            this.bDeptTextbox.Name = "bDeptTextbox";
            this.bDeptTextbox.Size = new System.Drawing.Size(176, 23);
            this.bDeptTextbox.TabIndex = 20;
            // 
            // BalancePage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bEndWith);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.bCode18Combox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.bCode13Combox);
            this.Controls.Add(this.bDeptTextbox);
            this.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "BalancePage";
            this.Size = new System.Drawing.Size(704, 404);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox bEndWith;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox bCode18Combox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox bCode13Combox;
        private System.Windows.Forms.TextBox bDeptTextbox;
    }
}
