﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;

namespace PosSetting.Pages
{
    public partial class KeyForm : Form
    {
        private String keycode;
        private String commandkey;
        private String commandname;
        private FileKeyBoardService service = FileKeyBoardService.getService();
        public KeyForm()
        {
            InitializeComponent();
        }

        public KeyForm(String keycode, String commandkey, String commandname) : this()
        {
            this.keycode = keycode;
            this.commandkey = commandkey;
            this.commandname = commandname;
            commandNameLabel.Text = "操作：" + commandname + "  键值：" + keycode;
            keycodeLable.Text = keycode;
        }

        private void KeyForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                updateKeyCode(commandkey, keycodeLable.Text);
                this.Close();
            }
            else
            {
                keycodeLable.Text = e.KeyCode.ToString();
            }
        }

        private void updateKeyCode(String commandkey, String keycode)
        {
            service.update(commandkey, keycode);
        }
    }
}
