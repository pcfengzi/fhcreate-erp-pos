﻿namespace PosSetting
{
    partial class SettingForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.baseButton5 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton4 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton3 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton2 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton1 = new YunLuPos.Com.BaseButton(this.components);
            this.shortBar1 = new YunLuPos.Com.ShortBar();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnTest = new YunLuPos.Com.BaseButton(this.components);
            this.btnSave = new YunLuPos.Com.BaseButton(this.components);
            this.labTitle = new System.Windows.Forms.Label();
            this.ContentPanel = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // baseButton5
            // 
            this.baseButton5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton5.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton5.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton5.ForeColor = System.Drawing.Color.White;
            this.baseButton5.Location = new System.Drawing.Point(9, 51);
            this.baseButton5.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton5.Name = "baseButton5";
            this.baseButton5.Size = new System.Drawing.Size(90, 30);
            this.baseButton5.TabIndex = 5;
            this.baseButton5.Text = "初始化";
            this.baseButton5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton5.UseVisualStyleBackColor = false;
            // 
            // baseButton4
            // 
            this.baseButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton4.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton4.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton4.ForeColor = System.Drawing.Color.White;
            this.baseButton4.Location = new System.Drawing.Point(9, 208);
            this.baseButton4.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton4.Name = "baseButton4";
            this.baseButton4.Size = new System.Drawing.Size(90, 30);
            this.baseButton4.TabIndex = 4;
            this.baseButton4.Text = "称重码设置";
            this.baseButton4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton4.UseVisualStyleBackColor = false;
            this.baseButton4.Click += new System.EventHandler(this.baseButton4_Click);
            // 
            // baseButton3
            // 
            this.baseButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton3.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton3.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton3.ForeColor = System.Drawing.Color.White;
            this.baseButton3.Location = new System.Drawing.Point(9, 169);
            this.baseButton3.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton3.Name = "baseButton3";
            this.baseButton3.Size = new System.Drawing.Size(90, 30);
            this.baseButton3.TabIndex = 3;
            this.baseButton3.Text = "键盘设置";
            this.baseButton3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton3.UseVisualStyleBackColor = false;
            this.baseButton3.Click += new System.EventHandler(this.baseButton3_Click);
            // 
            // baseButton2
            // 
            this.baseButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton2.ForeColor = System.Drawing.Color.White;
            this.baseButton2.Location = new System.Drawing.Point(9, 130);
            this.baseButton2.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton2.Name = "baseButton2";
            this.baseButton2.Size = new System.Drawing.Size(90, 30);
            this.baseButton2.TabIndex = 2;
            this.baseButton2.Text = "客显设置";
            this.baseButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton2.UseVisualStyleBackColor = false;
            this.baseButton2.Click += new System.EventHandler(this.baseButton2_Click);
            // 
            // baseButton1
            // 
            this.baseButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton1.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton1.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton1.ForeColor = System.Drawing.Color.White;
            this.baseButton1.Location = new System.Drawing.Point(9, 91);
            this.baseButton1.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton1.Name = "baseButton1";
            this.baseButton1.Size = new System.Drawing.Size(90, 30);
            this.baseButton1.TabIndex = 1;
            this.baseButton1.Text = "打印机设置";
            this.baseButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton1.UseVisualStyleBackColor = false;
            this.baseButton1.Click += new System.EventHandler(this.baseButton1_Click);
            // 
            // shortBar1
            // 
            this.shortBar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.shortBar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.shortBar1.Location = new System.Drawing.Point(0, 0);
            this.shortBar1.Name = "shortBar1";
            this.shortBar1.Size = new System.Drawing.Size(108, 528);
            this.shortBar1.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnTest);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(108, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(835, 50);
            this.panel1.TabIndex = 7;
            // 
            // btnTest
            // 
            this.btnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTest.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTest.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.btnTest.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnTest.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.btnTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTest.ForeColor = System.Drawing.Color.White;
            this.btnTest.Location = new System.Drawing.Point(635, 11);
            this.btnTest.Margin = new System.Windows.Forms.Padding(0);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(90, 30);
            this.btnTest.TabIndex = 1;
            this.btnTest.Text = "测试";
            this.btnTest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTest.UseVisualStyleBackColor = false;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnSave.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.btnSave.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(734, 11);
            this.btnSave.Margin = new System.Windows.Forms.Padding(0);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(90, 30);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "保存";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // labTitle
            // 
            this.labTitle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labTitle.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.labTitle.ForeColor = System.Drawing.Color.White;
            this.labTitle.Location = new System.Drawing.Point(9, 0);
            this.labTitle.Name = "labTitle";
            this.labTitle.Size = new System.Drawing.Size(99, 46);
            this.labTitle.TabIndex = 8;
            this.labTitle.Text = "收银台设置";
            this.labTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ContentPanel
            // 
            this.ContentPanel.BackColor = System.Drawing.Color.White;
            this.ContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContentPanel.Location = new System.Drawing.Point(108, 50);
            this.ContentPanel.Name = "ContentPanel";
            this.ContentPanel.Size = new System.Drawing.Size(835, 478);
            this.ContentPanel.TabIndex = 9;
            // 
            // SettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 528);
            this.Controls.Add(this.ContentPanel);
            this.Controls.Add(this.labTitle);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.baseButton5);
            this.Controls.Add(this.baseButton4);
            this.Controls.Add(this.baseButton3);
            this.Controls.Add(this.baseButton2);
            this.Controls.Add(this.baseButton1);
            this.Controls.Add(this.shortBar1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MinimizeBox = false;
            this.Name = "SettingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "收银台设置";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private YunLuPos.Com.ShortBar shortBar1;
        private YunLuPos.Com.BaseButton baseButton1;
        private YunLuPos.Com.BaseButton baseButton2;
        private YunLuPos.Com.BaseButton baseButton3;
        private YunLuPos.Com.BaseButton baseButton4;
        private YunLuPos.Com.BaseButton baseButton5;
        private System.Windows.Forms.Panel panel1;
        private YunLuPos.Com.BaseButton btnSave;
        private YunLuPos.Com.BaseButton btnTest;
        private System.Windows.Forms.Label labTitle;
        private System.Windows.Forms.Panel ContentPanel;
    }
}

