﻿using PosSetting.Pages;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PosSetting
{
    public partial class SettingForm : Form
    {
        BasePage current = null;
        public SettingForm()
        {
            InitializeComponent();
            this.btnTest.TextAlign = ContentAlignment.MiddleCenter;
            this.btnSave.TextAlign = ContentAlignment.MiddleCenter;
        }


        void switchPage(System.Windows.Forms.Control page)
        {
            this.ContentPanel.Controls.Clear();
            if (page != null)
            {
                page.Dock = DockStyle.Fill;
                this.ContentPanel.Controls.Add(page);
            }
        }

        private void baseButton1_Click(object sender, EventArgs e)
        {
            this.labTitle.Text = "打印机设置";
            current = new SettingPage();
            switchPage((Control)current);
        }
        private void baseButton2_Click(object sender, EventArgs e)
        {
            this.labTitle.Text = "客显设置";
            current = new DisplayPage();
            switchPage((Control)current);
        }

        private void baseButton3_Click(object sender, EventArgs e)
        {
            this.labTitle.Text = "键盘设置";
            current = new KeyBoardPage();
            switchPage((Control)current);
        }

        private void baseButton4_Click(object sender, EventArgs e)
        {
            this.labTitle.Text = "称重码设置";
            current = new BalancePage();
            switchPage((Control)current);
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            if (current != null)
            {
                current.test();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(current != null)
            {
                current.save();
            }
        }

        private void baseButton6_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.Exit();
        }

    }
}
