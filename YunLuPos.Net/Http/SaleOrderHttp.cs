﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.DB.Service;
using YunLuPos.Entity;

namespace YunLuPos.Net.Http
{
    public class SaleOrderHttp:HttpBase,SaleOrderNetService
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(SaleOrderHttp));
        SaleOrderService orderService = new SaleOrderService();
        GoodsService goodsService = new GoodsService();

        public long upload(SaleOrder saleOrder)
        {
            
           return doRequest<long>(saleOrder, "pos/saleorder/upload");
            
        }
    }
}
