﻿namespace YunLuPos
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.content = new System.Windows.Forms.Panel();
            this.mainContentPanel = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.goodsGrid = new YunLuPos.Com.BaseGrid(this.components);
            this.goodsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goodsCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sumAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.disAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceSource = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel2 = new System.Windows.Forms.Panel();
            this.memberInfoPanel1 = new YunLuPos.Com.MemberInfoPanel();
            this.paymentPanel1 = new YunLuPos.Com.PaymentPanel();
            this.preOrderInfoPanel1 = new YunLuPos.Com.PreOrderInfoPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.goodsInfoPanel1 = new YunLuPos.Com.GoodsInfoPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.topInfoPanel1 = new YunLuPos.Com.TopInfoPanel();
            this.shortPanel = new System.Windows.Forms.Panel();
            this.shortBar = new YunLuPos.Com.ShortBar();
            this.netCheckWorker = new System.ComponentModel.BackgroundWorker();
            this.content.SuspendLayout();
            this.mainContentPanel.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.goodsGrid)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.shortPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // content
            // 
            this.content.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.content.Controls.Add(this.mainContentPanel);
            this.content.Controls.Add(this.shortPanel);
            this.content.Dock = System.Windows.Forms.DockStyle.Fill;
            this.content.Location = new System.Drawing.Point(0, 0);
            this.content.Name = "content";
            this.content.Size = new System.Drawing.Size(784, 562);
            this.content.TabIndex = 0;
            // 
            // mainContentPanel
            // 
            this.mainContentPanel.Controls.Add(this.panel3);
            this.mainContentPanel.Controls.Add(this.panel2);
            this.mainContentPanel.Controls.Add(this.panel1);
            this.mainContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainContentPanel.Location = new System.Drawing.Point(94, 0);
            this.mainContentPanel.Name = "mainContentPanel";
            this.mainContentPanel.Size = new System.Drawing.Size(688, 560);
            this.mainContentPanel.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.goodsGrid);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 28);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(688, 442);
            this.panel3.TabIndex = 2;
            // 
            // goodsGrid
            // 
            this.goodsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.goodsGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.goodsGrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.goodsGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle1.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.goodsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.goodsGrid.ColumnHeadersHeight = 25;
            this.goodsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.goodsName,
            this.barCode,
            this.price,
            this.goodsCount,
            this.sumAmount,
            this.disAmount,
            this.priceSource});
            this.goodsGrid.CustomFoceColor = System.Drawing.Color.White;
            this.goodsGrid.CustomMainColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.goodsGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.goodsGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.goodsGrid.EnableHeadersVisualStyles = false;
            this.goodsGrid.GridColor = System.Drawing.Color.White;
            this.goodsGrid.Location = new System.Drawing.Point(0, 0);
            this.goodsGrid.MultiSelect = false;
            this.goodsGrid.Name = "goodsGrid";
            this.goodsGrid.ReadOnly = true;
            this.goodsGrid.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.goodsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.goodsGrid.RowTemplate.Height = 25;
            this.goodsGrid.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.goodsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.goodsGrid.Size = new System.Drawing.Size(688, 442);
            this.goodsGrid.TabIndex = 2;
            this.goodsGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.goodsGrid_CellFormatting);
            // 
            // goodsName
            // 
            this.goodsName.DataPropertyName = "goodsName";
            this.goodsName.FillWeight = 169.1647F;
            this.goodsName.HeaderText = "名称";
            this.goodsName.Name = "goodsName";
            this.goodsName.ReadOnly = true;
            // 
            // barCode
            // 
            this.barCode.DataPropertyName = "barCode";
            this.barCode.FillWeight = 121.8274F;
            this.barCode.HeaderText = "条码";
            this.barCode.Name = "barCode";
            this.barCode.ReadOnly = true;
            // 
            // price
            // 
            this.price.DataPropertyName = "disPrice";
            this.price.FillWeight = 77.2519F;
            this.price.HeaderText = "价格";
            this.price.Name = "price";
            this.price.ReadOnly = true;
            // 
            // goodsCount
            // 
            this.goodsCount.DataPropertyName = "goodsCount";
            this.goodsCount.FillWeight = 77.2519F;
            this.goodsCount.HeaderText = "数量";
            this.goodsCount.Name = "goodsCount";
            this.goodsCount.ReadOnly = true;
            // 
            // sumAmount
            // 
            this.sumAmount.DataPropertyName = "payAmount";
            this.sumAmount.FillWeight = 77.2519F;
            this.sumAmount.HeaderText = "小计";
            this.sumAmount.Name = "sumAmount";
            this.sumAmount.ReadOnly = true;
            // 
            // disAmount
            // 
            this.disAmount.DataPropertyName = "disAmount";
            this.disAmount.FillWeight = 77.2519F;
            this.disAmount.HeaderText = "优惠";
            this.disAmount.Name = "disAmount";
            this.disAmount.ReadOnly = true;
            // 
            // priceSource
            // 
            this.priceSource.DataPropertyName = "priceSource";
            this.priceSource.HeaderText = "价格来源";
            this.priceSource.Name = "priceSource";
            this.priceSource.ReadOnly = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.memberInfoPanel1);
            this.panel2.Controls.Add(this.paymentPanel1);
            this.panel2.Controls.Add(this.preOrderInfoPanel1);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.goodsInfoPanel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 470);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(688, 90);
            this.panel2.TabIndex = 1;
            // 
            // memberInfoPanel1
            // 
            this.memberInfoPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.memberInfoPanel1.Location = new System.Drawing.Point(389, 1);
            this.memberInfoPanel1.Name = "memberInfoPanel1";
            this.memberInfoPanel1.Size = new System.Drawing.Size(130, 88);
            this.memberInfoPanel1.TabIndex = 4;
            // 
            // paymentPanel1
            // 
            this.paymentPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.paymentPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.paymentPanel1.Location = new System.Drawing.Point(525, 2);
            this.paymentPanel1.Name = "paymentPanel1";
            this.paymentPanel1.Size = new System.Drawing.Size(164, 88);
            this.paymentPanel1.TabIndex = 3;
            // 
            // preOrderInfoPanel1
            // 
            this.preOrderInfoPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.preOrderInfoPanel1.Location = new System.Drawing.Point(239, 3);
            this.preOrderInfoPanel1.Name = "preOrderInfoPanel1";
            this.preOrderInfoPanel1.Size = new System.Drawing.Size(144, 88);
            this.preOrderInfoPanel1.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(688, 1);
            this.panel4.TabIndex = 1;
            // 
            // goodsInfoPanel1
            // 
            this.goodsInfoPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.goodsInfoPanel1.Location = new System.Drawing.Point(0, 3);
            this.goodsInfoPanel1.Name = "goodsInfoPanel1";
            this.goodsInfoPanel1.Size = new System.Drawing.Size(239, 88);
            this.goodsInfoPanel1.TabIndex = 0;
            this.goodsInfoPanel1.CodeSearchEvent += new YunLuPos.Com.GoodsInfoPanel.CodeSearchHandle(this.goodsInfoPanel1_CodeSearchEvent);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.topInfoPanel1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(688, 28);
            this.panel1.TabIndex = 0;
            // 
            // topInfoPanel1
            // 
            this.topInfoPanel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.topInfoPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.topInfoPanel1.Location = new System.Drawing.Point(0, 0);
            this.topInfoPanel1.Name = "topInfoPanel1";
            this.topInfoPanel1.Size = new System.Drawing.Size(688, 28);
            this.topInfoPanel1.TabIndex = 0;
            // 
            // shortPanel
            // 
            this.shortPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.shortPanel.Controls.Add(this.shortBar);
            this.shortPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.shortPanel.Location = new System.Drawing.Point(0, 0);
            this.shortPanel.Name = "shortPanel";
            this.shortPanel.Size = new System.Drawing.Size(94, 560);
            this.shortPanel.TabIndex = 0;
            // 
            // shortBar
            // 
            this.shortBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.shortBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shortBar.Location = new System.Drawing.Point(0, 0);
            this.shortBar.Name = "shortBar";
            this.shortBar.Size = new System.Drawing.Size(92, 558);
            this.shortBar.TabIndex = 1;
            this.shortBar.OptionTouchEvent += new YunLuPos.Com.ShortBar.OptionTouchHandle(this.shortBar1_OptionTouchEvent);
            // 
            // netCheckWorker
            // 
            this.netCheckWorker.WorkerReportsProgress = true;
            this.netCheckWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.netCheckWorker_DoWork);
            this.netCheckWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.netCheckWorker_ProgressChanged);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.content);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "富海云创收银台";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.content.ResumeLayout(false);
            this.mainContentPanel.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.goodsGrid)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.shortPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel content;
        private System.Windows.Forms.Panel shortPanel;
        private Com.ShortBar shortBar;
        private System.Windows.Forms.Panel mainContentPanel;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private Com.TopInfoPanel topInfoPanel1;
        private System.Windows.Forms.Panel panel4;
        private Com.PreOrderInfoPanel preOrderInfoPanel1;
        private Com.PaymentPanel paymentPanel1;
        private Com.MemberInfoPanel memberInfoPanel1;
        public Com.GoodsInfoPanel goodsInfoPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn goodsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn barCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn price;
        private System.Windows.Forms.DataGridViewTextBoxColumn goodsCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn sumAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn disAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceSource;
        public Com.BaseGrid goodsGrid;
        private System.ComponentModel.BackgroundWorker netCheckWorker;
    }
}