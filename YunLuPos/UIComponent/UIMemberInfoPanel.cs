﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;

namespace YunLuPos.UIComponent
{
    public partial class UIMemberInfoPanel : UserControl
    {
        public UIMemberInfoPanel()
        {
            InitializeComponent();
        }

        public void setMebInfo(Member meb)
        {
            if (meb == null)
            {
                this.mebCode.Text = "无";
                this.mebName.Text = "无";
            }
            else
            {
                this.mebCode.Text = meb.mebCode;
                this.mebName.Text = meb.mebName;
            }
        }
    }
}
