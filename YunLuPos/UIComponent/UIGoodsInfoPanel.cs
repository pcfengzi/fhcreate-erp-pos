﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Entity;

namespace YunLuPos.UIComponent
{
    public partial class UIGoodsInfoPanel : UserControl
    {

        public delegate void CodeSearchHandle(String value, UIGoodsInfoPanel handel);

        public event CodeSearchHandle CodeSearchEvent;

        public UIGoodsInfoPanel()
        {
            InitializeComponent();
        }

        public void setFocus()
        {
            this.codeInput.Focus();
        }

        public void setGoods(Goods goods)
        {
            this.codeInput.Text = "";
            if (goods == null)
            {
                this.goodsName.Text = "无";
                this.specs.Text = "无";
                this.unit.Text = "无";
                this.price.Text = "0.00";
            }
            else
            {
                this.goodsName.Text = goods.goodsName;
                this.specs.Text = goods.specs;
                this.unit.Text = goods.unitName;
                Console.WriteLine(String.Format("{0:C}", goods.salePrice));
                this.price.Text = String.Format("{0:C}", goods.salePrice);

            }
        }

        private void codeInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            const char Delete = (char)8;
            const char Enter = (char)13;
            if (e.KeyChar == Enter)
            {
                CodeSearchEvent?.Invoke(codeInput.Text, this);
                e.Handled = true;
            }
            else if (!(e.KeyChar >= '0' && e.KeyChar <= '9') && e.KeyChar != Delete)
            {
                e.Handled = true;
            }
        }
    }
}
