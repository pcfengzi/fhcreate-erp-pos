﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Printer;
using YunLuPos.SellView;
using YunLuPos.Utils;

namespace YunLuPos.Command
{
    /**
     * 获取光标命令
     * */
    public class OpenBoxCommand : CommandInterface
    {
        CashierService cashierService = new CashierService();
        public void exec(Form handel, Cashier cashier)
        {
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                doOpen();
            }
            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                String nedPwd = InIUtil.ReadInIData(InIUtil.GenSection, InIUtil.G_BOX_PWD, "1");
                if ("1".Equals(nedPwd))
                {
                    String pwd = "";
                    
                    if(smf.InputPasswordDialog(ref pwd, true, "请输入密码",smf.Style))
                    {
                        Cashier c = cashierService.doLogin(StaticInfoHoder.commInfo.cashierCode, pwd);
                        if (c != null)
                        {
                            doOpen();
                        }
                        else
                        {
                            smf.ShowInfoDialog("密码错误！",smf.Style);
                        }
                    }
                }else
                {
                    doOpen();
                }
                
            }
        }

        void doOpen()
        {
            PrintBase pb = PrinterManager.getPrinter();
            if (pb != null)
            {
                pb.initPrinter();
                pb.open();
                if (pb is LPTPrinter)
                {
                    pb.writeLine(" ");
                }
                pb.openMoneyBox();
                pb.Dispose();
            }
        }

        public string getKey()
        {
            return Names.OpenBox.ToString();
        }

        public bool needAuth()
        {
            return false;
        }
    }
}
