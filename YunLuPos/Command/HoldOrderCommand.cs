﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.SellView;

namespace YunLuPos.Command
{
    class HoldOrderCommand : CommandInterface
    {
        private SaleOrderService orderService = new SaleOrderService();
        public void exec(Form handel, Cashier auther)
        {
            if (DynamicInfoHoder.currentOrder.goods == null || DynamicInfoHoder.currentOrder.goods.Count <= 0)
            {
                return;
            }
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                ConfirmDialog confirm = new ConfirmDialog("挂单？");
                if( confirm.ShowDialog() == DialogResult.OK)
                {
                    bool b = orderService.hoderOrder(DynamicInfoHoder.currentOrder,true);
                    if (b)
                    {
                        mf.createNewOrder();
                    }
                }
            }

            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                if (smf.goodsGrid.CurrentRow == null)
                {
                    return;
                }
                if (smf.ShowAskDialog("挂单", "确认挂起当前单据?", smf.Style))
                {
                    bool b = orderService.hoderOrder(DynamicInfoHoder.currentOrder, true);
                    if (b)
                    {
                        smf.createNewOrder();
                    }
                }
            }
        }

        public string getKey()
        {
            return Names.HoldOrder.ToString();
        }

        public bool needAuth()
        {
            return false;
        }
    }
}
