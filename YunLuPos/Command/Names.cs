﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YunLuPos.Command
{
    public enum Names
    {
        BarCodeFocus,
        SaleOrderDirty,
        SingleDel,
        ChangeCount,
        ChangePrice,
        Payment,
        HoldOrder,
        TakeOrder,
        Refund,
        LockPos,
        Sync,
        Weight,
        ShowShortBar,
        OrderSelect,
        OpenBox,
        NumPlus,
        NumLess,
        Member
    }
}
