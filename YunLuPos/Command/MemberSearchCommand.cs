﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.DB.Service;
using YunLuPos.DB.Utils;
using YunLuPos.Entity;
using YunLuPos.Entity.Constant;
using YunLuPos.Net;
using YunLuPos.Net.DTO;
using YunLuPos.SellView;

namespace YunLuPos.Command
{
    class MemberSearchCommand : CommandInterface
    {
        MebNetService mebHttp =  NetServiceManager.getMebNetService();
        SaleOrderService orderService = new SaleOrderService();
        public void exec(Form handel, Cashier auther)
        {
            MainForm mf = handel as MainForm;
            if (mf != null)
            {
                
            }
            SellMainForm smf = handel as SellMainForm;
            if (smf != null)
            {
                String value = null;
                if (smf.InputStringDialog(ref value,true,"请输入会员手机号或编号",smf.Style))
                {
                    MebSearchInfo dto = new MebSearchInfo();
                    dto.cliqueCode = StaticInfoHoder.commInfo.cliqueCode;
                    dto.branchCode = StaticInfoHoder.commInfo.shopCode;
                    dto.cashierCode = StaticInfoHoder.commInfo.cashierCode;
                    dto.searchString = value;
                    Member m = mebHttp.search(dto);
                    if(m != null)
                    {
                        Boolean b = orderService.setMemberInfo(DynamicInfoHoder.currentOrder, m);
                        if (b)
                        {
                            smf.displayCurrentOrder(true,false); //刷新当前单据界面显示
                        }
                    }
                }
            }


        }

        public string getKey()
        {
            return Names.Member.ToString();
        }

        public bool needAuth()
        {
            return false;
        }
    }
}
