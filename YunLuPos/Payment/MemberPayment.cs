﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.SellView;
using YunLuPos.View;

namespace YunLuPos.Payment
{
    public class MemberPayment : PaymentInterface
    {
        private CodesService codesService = new CodesService();
        private SaleOrderAccountService accountService = new SaleOrderAccountService();
        PayTypeService payTypeService = new PayTypeService();
        String mebPayInfo = "";
        public string getKey()
        {
            return "MEMBER";
        }

        public ResultMessage doPay(SaleOrder order, PayType payType, PaymentState state, double inputAmount)
        {
            //扫码支付
            String outTradeNo = codesService.genPayOutTradeNo(order.orderCode);
            SellMemberPay sacnForm = new SellMemberPay(order.orderCode,outTradeNo, inputAmount);
            DialogResult result = sacnForm.ShowDialog();
            if(result == DialogResult.OK)
            {
                Member m = sacnForm.member;
               
                if(m != null)
                {
                    Decimal payAmt = Convert.ToDecimal(inputAmount);
                    Decimal lessAmt = m.amount - payAmt;
                    mebPayInfo = m.mebName + "," + m.phoneNo + "," + lessAmt.ToString("F2");
                }
                
                String errorMessage = accountService.addAccount(order, payType, state, inputAmount,null, mebPayInfo);
                if (errorMessage != null)
                {
                    return ResultMessage.createError(errorMessage);
                }
                else
                {
                    return ResultMessage.createSuccess();
                }
            }
            else
            {
                return ResultMessage.createError("刷卡取消");
            }
        }

        public List<string> print()
        {
            List<String> list = new List<string>();
            list.Add("会员卡余额："+ mebPayInfo);
            return list;
        }
    }
}
