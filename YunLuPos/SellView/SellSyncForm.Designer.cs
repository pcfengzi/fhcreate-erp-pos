﻿namespace YunLuPos.SellView
{
    partial class SellSyncForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.syncProcess = new Sunny.UI.UIProcessBar();
            this.uiLabel1 = new Sunny.UI.UILabel();
            this.taskMessage = new Sunny.UI.UILabel();
            this.syncWorker = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // syncProcess
            // 
            this.syncProcess.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.syncProcess.Location = new System.Drawing.Point(26, 114);
            this.syncProcess.MinimumSize = new System.Drawing.Size(70, 23);
            this.syncProcess.Name = "syncProcess";
            this.syncProcess.Size = new System.Drawing.Size(350, 29);
            this.syncProcess.TabIndex = 0;
            this.syncProcess.Text = "0.0%";
            // 
            // uiLabel1
            // 
            this.uiLabel1.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.uiLabel1.Location = new System.Drawing.Point(25, 83);
            this.uiLabel1.Name = "uiLabel1";
            this.uiLabel1.Size = new System.Drawing.Size(53, 23);
            this.uiLabel1.TabIndex = 1;
            this.uiLabel1.Text = "同步:";
            this.uiLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // taskMessage
            // 
            this.taskMessage.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.taskMessage.Location = new System.Drawing.Point(73, 83);
            this.taskMessage.Name = "taskMessage";
            this.taskMessage.Size = new System.Drawing.Size(303, 23);
            this.taskMessage.TabIndex = 2;
            this.taskMessage.Text = "*";
            this.taskMessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // syncWorker
            // 
            this.syncWorker.WorkerReportsProgress = true;
            this.syncWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.syncWorker_DoWork);
            this.syncWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.syncWorker_ProgressChanged);
            this.syncWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.syncWorker_RunWorkerCompleted);
            // 
            // SellSyncForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(396, 204);
            this.Controls.Add(this.taskMessage);
            this.Controls.Add(this.uiLabel1);
            this.Controls.Add(this.syncProcess);
            this.EscClose = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SellSyncForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "数据通讯";
            this.Load += new System.EventHandler(this.SellSyncForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Sunny.UI.UIProcessBar syncProcess;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel taskMessage;
        private System.ComponentModel.BackgroundWorker syncWorker;
    }
}