﻿namespace YunLuPos.SellView
{
    partial class SellMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.goodsGrid = new Sunny.UI.UIDataGridView();
            this.goodsName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.barCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.disPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.goodsCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.payAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.disAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.priceSource = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.netCheckWorker = new System.ComponentModel.BackgroundWorker();
            this.shortPanel = new Sunny.UI.UIPanel();
            this.flowPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.uiWeightDisplayPanel1 = new YunLuPos.UIComponent.UIWeightDisplayPanel();
            this.paymentPanel = new YunLuPos.UIComponent.UIPaymentPanel();
            this.uiMemberInfoPanel1 = new YunLuPos.UIComponent.UIMemberInfoPanel();
            this.preOrderInfoPanel = new YunLuPos.UIComponent.UIPreOrderInfoPanel();
            this.goodsInfoPanel = new YunLuPos.UIComponent.UIGoodsInfoPanel();
            this.topInfoPanel = new YunLuPos.UIComponent.UITopInfoPanel();
            ((System.ComponentModel.ISupportInitialize)(this.goodsGrid)).BeginInit();
            this.shortPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // goodsGrid
            // 
            this.goodsGrid.AllowUserToAddRows = false;
            this.goodsGrid.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(235)))), ((int)(((byte)(243)))), ((int)(((byte)(255)))));
            this.goodsGrid.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.goodsGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.goodsGrid.BackgroundColor = System.Drawing.Color.White;
            this.goodsGrid.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.goodsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.goodsGrid.ColumnHeadersHeight = 32;
            this.goodsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.goodsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.goodsName,
            this.barCode,
            this.disPrice,
            this.goodsCount,
            this.payAmount,
            this.disAmount,
            this.priceSource});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(200)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(48)))), ((int)(((byte)(48)))), ((int)(((byte)(48)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.goodsGrid.DefaultCellStyle = dataGridViewCellStyle3;
            this.goodsGrid.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.goodsGrid.EnableHeadersVisualStyles = false;
            this.goodsGrid.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.goodsGrid.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(160)))), ((int)(((byte)(255)))));
            this.goodsGrid.Location = new System.Drawing.Point(4, 102);
            this.goodsGrid.Name = "goodsGrid";
            this.goodsGrid.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("微软雅黑", 12F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.goodsGrid.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            this.goodsGrid.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.goodsGrid.RowTemplate.Height = 29;
            this.goodsGrid.SelectedIndex = -1;
            this.goodsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.goodsGrid.ShowGridLine = true;
            this.goodsGrid.Size = new System.Drawing.Size(681, 386);
            this.goodsGrid.TabIndex = 3;
            this.goodsGrid.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.goodsGrid_CellFormatting);
            this.goodsGrid.RowPostPaint += new System.Windows.Forms.DataGridViewRowPostPaintEventHandler(this.goodsGrid_RowPostPaint);
            this.goodsGrid.Paint += new System.Windows.Forms.PaintEventHandler(this.goodsGrid_Paint);
            // 
            // goodsName
            // 
            this.goodsName.DataPropertyName = "goodsName";
            this.goodsName.HeaderText = "名称";
            this.goodsName.Name = "goodsName";
            this.goodsName.ReadOnly = true;
            this.goodsName.Width = 170;
            // 
            // barCode
            // 
            this.barCode.DataPropertyName = "barCode";
            this.barCode.HeaderText = "条码";
            this.barCode.Name = "barCode";
            this.barCode.ReadOnly = true;
            this.barCode.Width = 130;
            // 
            // disPrice
            // 
            this.disPrice.DataPropertyName = "disPrice";
            this.disPrice.HeaderText = "价格";
            this.disPrice.Name = "disPrice";
            this.disPrice.ReadOnly = true;
            this.disPrice.Width = 60;
            // 
            // goodsCount
            // 
            this.goodsCount.DataPropertyName = "goodsCount";
            this.goodsCount.HeaderText = "数量";
            this.goodsCount.Name = "goodsCount";
            this.goodsCount.ReadOnly = true;
            this.goodsCount.Width = 60;
            // 
            // payAmount
            // 
            this.payAmount.DataPropertyName = "payAmount";
            this.payAmount.HeaderText = "小计";
            this.payAmount.Name = "payAmount";
            this.payAmount.ReadOnly = true;
            this.payAmount.Width = 60;
            // 
            // disAmount
            // 
            this.disAmount.DataPropertyName = "disAmount";
            this.disAmount.HeaderText = "优惠";
            this.disAmount.Name = "disAmount";
            this.disAmount.ReadOnly = true;
            this.disAmount.Width = 60;
            // 
            // priceSource
            // 
            this.priceSource.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.priceSource.DataPropertyName = "priceSource";
            this.priceSource.HeaderText = "价格来源";
            this.priceSource.Name = "priceSource";
            this.priceSource.ReadOnly = true;
            // 
            // netCheckWorker
            // 
            this.netCheckWorker.WorkerReportsProgress = true;
            this.netCheckWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.netCheckWorker_DoWork);
            this.netCheckWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.netCheckWorker_ProgressChanged);
            this.netCheckWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.netCheckWorker_RunWorkerCompleted);
            // 
            // shortPanel
            // 
            this.shortPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.shortPanel.Controls.Add(this.flowPanel);
            this.shortPanel.Font = new System.Drawing.Font("微软雅黑", 12F);
            this.shortPanel.Location = new System.Drawing.Point(687, 102);
            this.shortPanel.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.shortPanel.MinimumSize = new System.Drawing.Size(1, 1);
            this.shortPanel.Name = "shortPanel";
            this.shortPanel.Size = new System.Drawing.Size(108, 386);
            this.shortPanel.TabIndex = 9;
            this.shortPanel.Text = null;
            // 
            // flowPanel
            // 
            this.flowPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flowPanel.Location = new System.Drawing.Point(2, 6);
            this.flowPanel.Name = "flowPanel";
            this.flowPanel.Size = new System.Drawing.Size(104, 375);
            this.flowPanel.TabIndex = 0;
            // 
            // uiWeightDisplayPanel1
            // 
            this.uiWeightDisplayPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uiWeightDisplayPanel1.Location = new System.Drawing.Point(392, 492);
            this.uiWeightDisplayPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.uiWeightDisplayPanel1.Name = "uiWeightDisplayPanel1";
            this.uiWeightDisplayPanel1.Size = new System.Drawing.Size(121, 105);
            this.uiWeightDisplayPanel1.TabIndex = 8;
            // 
            // paymentPanel
            // 
            this.paymentPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.paymentPanel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.paymentPanel.Location = new System.Drawing.Point(655, 492);
            this.paymentPanel.Margin = new System.Windows.Forms.Padding(5);
            this.paymentPanel.Name = "paymentPanel";
            this.paymentPanel.Size = new System.Drawing.Size(140, 105);
            this.paymentPanel.TabIndex = 7;
            // 
            // uiMemberInfoPanel1
            // 
            this.uiMemberInfoPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiMemberInfoPanel1.Location = new System.Drawing.Point(519, 492);
            this.uiMemberInfoPanel1.Margin = new System.Windows.Forms.Padding(5);
            this.uiMemberInfoPanel1.Name = "uiMemberInfoPanel1";
            this.uiMemberInfoPanel1.Size = new System.Drawing.Size(133, 105);
            this.uiMemberInfoPanel1.TabIndex = 6;
            // 
            // preOrderInfoPanel
            // 
            this.preOrderInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.preOrderInfoPanel.Location = new System.Drawing.Point(266, 492);
            this.preOrderInfoPanel.Margin = new System.Windows.Forms.Padding(5);
            this.preOrderInfoPanel.Name = "preOrderInfoPanel";
            this.preOrderInfoPanel.Size = new System.Drawing.Size(123, 105);
            this.preOrderInfoPanel.TabIndex = 5;
            // 
            // goodsInfoPanel
            // 
            this.goodsInfoPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.goodsInfoPanel.Location = new System.Drawing.Point(3, 492);
            this.goodsInfoPanel.Margin = new System.Windows.Forms.Padding(5);
            this.goodsInfoPanel.Name = "goodsInfoPanel";
            this.goodsInfoPanel.Size = new System.Drawing.Size(260, 105);
            this.goodsInfoPanel.TabIndex = 0;
            this.goodsInfoPanel.CodeSearchEvent += new YunLuPos.UIComponent.UIGoodsInfoPanel.CodeSearchHandle(this.goodsInfoPanel_CodeSearchEvent);
            // 
            // topInfoPanel
            // 
            this.topInfoPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topInfoPanel.Location = new System.Drawing.Point(0, 35);
            this.topInfoPanel.Margin = new System.Windows.Forms.Padding(5);
            this.topInfoPanel.Name = "topInfoPanel";
            this.topInfoPanel.Size = new System.Drawing.Size(800, 66);
            this.topInfoPanel.TabIndex = 0;
            // 
            // SellMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.shortPanel);
            this.Controls.Add(this.uiWeightDisplayPanel1);
            this.Controls.Add(this.paymentPanel);
            this.Controls.Add(this.uiMemberInfoPanel1);
            this.Controls.Add(this.preOrderInfoPanel);
            this.Controls.Add(this.goodsInfoPanel);
            this.Controls.Add(this.goodsGrid);
            this.Controls.Add(this.topInfoPanel);
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.KeyPreview = true;
            this.Name = "SellMainForm";
            this.Text = "富海云创收银台";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SellMainForm_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SellMainForm_FormClosed);
            this.Load += new System.EventHandler(this.SellMainForm_Load);
            this.Shown += new System.EventHandler(this.SellMainForm_Shown);
            this.ResizeEnd += new System.EventHandler(this.SellMainForm_ResizeEnd);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SellMainForm_KeyDown);
            this.Resize += new System.EventHandler(this.SellMainForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.goodsGrid)).EndInit();
            this.shortPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private UIComponent.UITopInfoPanel topInfoPanel;
        private UIComponent.UIPreOrderInfoPanel preOrderInfoPanel;
        private UIComponent.UIMemberInfoPanel uiMemberInfoPanel1;
        private UIComponent.UIPaymentPanel paymentPanel;
        public UIComponent.UIGoodsInfoPanel goodsInfoPanel;
        public Sunny.UI.UIDataGridView goodsGrid;
        private System.ComponentModel.BackgroundWorker netCheckWorker;
        public UIComponent.UIWeightDisplayPanel uiWeightDisplayPanel1;
        public System.Windows.Forms.FlowLayoutPanel flowPanel;
        public Sunny.UI.UIPanel shortPanel;
        private System.Windows.Forms.DataGridViewTextBoxColumn goodsName;
        private System.Windows.Forms.DataGridViewTextBoxColumn barCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn disPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn goodsCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn payAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn disAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceSource;
    }
}