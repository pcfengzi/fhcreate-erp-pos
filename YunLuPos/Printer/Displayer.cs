﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using YunLuPos.Utils;

namespace YunLuPos.Printer
{
    /// <summary>
    /// 客显操作类
    /// </summary>

    public class Displayer
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(Displayer));

        SerialPort outSerialPort = new SerialPort();
        public Displayer()
        {
            //初始化客显端口
            try
            {
                initOutPort();
            }catch(Exception ex)
            {
                logger.Error("初始化客显异常",ex);
            }
        }


        void initOutPort()
        {
            
            String OUTCOM_PORT = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComPort);
            String OUTCOM_RATE = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComRate);
            String OUTCOM_BIT = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComBit);
            String OUTCOM_STOPBIT = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComStopBit);
            String OUTCOM_CHECKBIT = InIUtil.ReadInIData(InIUtil.DisplaySection, InIUtil.DisplayComCheckBit);
            //设置参数
            outSerialPort.PortName = OUTCOM_PORT;
            outSerialPort.BaudRate = Int32.Parse(OUTCOM_RATE); //串行波特率
            outSerialPort.DataBits = Int32.Parse(OUTCOM_BIT); //每个字节的标准数据位长度
            outSerialPort.StopBits = StopBits.One; //设置每个字节的标准停止位数
            if ("1".Equals(OUTCOM_STOPBIT))
            {
                outSerialPort.StopBits = StopBits.One;
            }
            else if ("1.5".Equals(OUTCOM_STOPBIT))
            {
                outSerialPort.StopBits = StopBits.OnePointFive;

            }
            else if ("2".Equals(OUTCOM_STOPBIT))
            {
                outSerialPort.StopBits = StopBits.Two;
            }
            outSerialPort.Parity = Parity.None; //设置奇偶校验检查协议
            if ("Even".Equals(OUTCOM_CHECKBIT))
            {
                outSerialPort.Parity = Parity.Even;
            }
            else if ("Mark".Equals(OUTCOM_CHECKBIT))
            {
                outSerialPort.Parity = Parity.Mark;
            }
            else if ("Odd".Equals(OUTCOM_CHECKBIT))
            {
                outSerialPort.Parity = Parity.Odd;
            }
            else if ("Space".Equals(OUTCOM_CHECKBIT))
            {
                outSerialPort.Parity = Parity.Space;
            }

        }

        
        /// <summary>
        /// 显示一个字符到客显
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>        
        public bool write(String content,int type)
        {
            try
            {
                outSerialPort.Open();
            }
            catch (Exception ex)
            {
                logger.Error("打开客显端口异常", ex);
                Console.WriteLine(ex);
            }
            Console.WriteLine(outSerialPort.IsOpen);
            if (outSerialPort != null && outSerialPort.IsOpen)
            {
                try
                {
                    outSerialPort.WriteLine(((char)12).ToString());

                    String led = ((char)27).ToString() +
                        ((char)115).ToString() +
                        ((char)type).ToString();

                    outSerialPort.WriteLine(led);
                        
                    //单价


                    //合计

                    var data = ((char)27).ToString() +
                        ((char)81).ToString() +
                        ((char)65).ToString() +
                        content+
                        ((char)13).ToString();
                    outSerialPort.WriteLine(data);
                }
                catch (Exception e)
                {
                    logger.Error("客显写数据异常", e);
                    return false;
                }
                finally
                {
                    outSerialPort.Close();
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool write(byte[] b)
        {
            try
            {
                outSerialPort.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            Console.WriteLine(outSerialPort.IsOpen);
            if (outSerialPort != null && outSerialPort.IsOpen)
            {
                try
                {
                    
                    Console.WriteLine(b.Length);
                    outSerialPort.Write(b, 0, b.Length);
                }
                catch (Exception e)
                {
                    return false;
                }
                finally
                {
                    outSerialPort.Close();
                }
                return true;
            }
            else
            {
                return false;
            }

        }

    }
}
