﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Net;
using YunLuPos.Net.Http;
using YunLuPos.Worker;

namespace YunLuPos
{
    public partial class LoginInitForm : Form
    {
        LoadServerDataWorker serverDataWorker = new LoadServerDataWorker();
        private CashierService cashierService = new CashierService();
        public LoginInitForm()
        {
            InitializeComponent();
        }

        private void pwdInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if(this.pwdInput.Text != null && !"".Equals(this.pwdInput.Text))
                {
                    this.codeInput.Focus();
                    bool b = doLogin();
                    this.codeInput.Text = "";
                    this.pwdInput.Text = "";
                    this.codeInput.Enabled = true;
                    this.codeInput.Focus();
                    if (b)
                    {
                        MainForm main = new MainForm(this);
                        main.Show();
                        this.Hide();
                    }
                }
                else
                {
                    this.codeInput.Focus();
                }
                
                e.Handled = true;
            }
        }

        private void codeInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13 && this.codeInput.Text != null && !"".Equals(this.codeInput.Text))
            {
                pwdInput.Focus();
            }
        }

        public void setLockInfo(String code)
        {
            this.codeInput.Text = code;
            this.codeInput.Enabled = false;
            this.pwdInput.Focus();
        }

        Boolean doLogin()
        {
            Cashier cashier = cashierService.doLogin(this.codeInput.Text.Trim(),this.pwdInput.Text.Trim());
            if(cashier != null)
            {
                StaticInfoHoder.commInfo.cashierCode = cashier.cashierCode;
                StaticInfoHoder.commInfo.cashierName = cashier.cashierName;
            }
            return cashier != null;
        }

        private void LoginInitForm_Load(object sender, EventArgs e)
        {
            this.versionLabel.Text = "版本：" + StaticInfoHoder.version ;
            this.codeInput.Enabled = false;
            this.pwdInput.Enabled = false;
            downWorker.RunWorkerAsync();
        }

        private void downWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.taskMessage.Text = e.UserState.ToString();
        }

        private void downWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            serverDataWorker.syncWithReport(downWorker);   
        }

        private void downWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Console.WriteLine("执行完成");
            this.codeInput.Enabled = true;
            this.pwdInput.Enabled = true;
            this.codeInput.Focus();
        }
    }
}
