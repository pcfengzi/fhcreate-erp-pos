﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using YunLuPos.DB.Service;
using YunLuPos.Entity;
using YunLuPos.Net;
using YunLuPos.Net.Http;

namespace YunLuPos.Worker
{
    public class NetChecker
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(SyncOrderWorker));
        private CommonNetService netService = NetServiceManager.getCommonNetService();

        public void asynStart()
        {
            Thread thread = new Thread(new ThreadStart(ThreadMethod));//创建线程
            thread.IsBackground = true;
            thread.Name = "NetCheck";
            thread.Start();
        }

        void ThreadMethod()
        {
            while (true)
            {
                String t = netService.heartbeat();
                DynamicInfoHoder.netIsAlive = t != null;
                Thread.Sleep(10 * 1000);
            }
        }

        

    }
}
