﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using YunLuPos.Com;
using YunLuPos.Worker;

namespace YunLuPos.View
{
    public partial class SyncForm : BaseDialogForm
    {
        LoadServerDataWorker serverDataWorker = new LoadServerDataWorker();
        public SyncForm()
        {
            InitializeComponent();
        }


        public new void BaseDialogForm_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape && !syncWorker.IsBusy)
            {
                this.DialogResult = DialogResult.Cancel;
                this.Close();
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            serverDataWorker.syncWithReport(syncWorker);
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            this.textBox1.Text = this.textBox1.Text + "\r\n" + e.UserState.ToString();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void SyncForm_Load(object sender, EventArgs e)
        {
            syncWorker.RunWorkerAsync();
        }

        private void baseButton2_Click(object sender, EventArgs e)
        {
            if (!syncWorker.IsBusy)
            {
                this.Close();
            }
        }
    }
}
