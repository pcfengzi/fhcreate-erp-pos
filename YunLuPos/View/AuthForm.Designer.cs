﻿namespace YunLuPos.View
{
    partial class AuthForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.baseLabel1 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel2 = new YunLuPos.Com.BaseLabel(this.components);
            this.codeInput = new YunLuPos.Com.BaseTextInput(this.components);
            this.pwdInput = new YunLuPos.Com.BaseTextInput(this.components);
            this.messageLabel = new YunLuPos.Com.BaseLabel(this.components);
            this.baseButton2 = new YunLuPos.Com.BaseButton(this.components);
            this.baseButton1 = new YunLuPos.Com.BaseButton(this.components);
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Size = new System.Drawing.Size(316, 41);
            this.label1.Text = "当前操作无权限需授权";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Size = new System.Drawing.Size(316, 213);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.baseButton2);
            this.panel3.Controls.Add(this.baseButton1);
            this.panel3.Controls.Add(this.messageLabel);
            this.panel3.Controls.Add(this.pwdInput);
            this.panel3.Controls.Add(this.codeInput);
            this.panel3.Controls.Add(this.baseLabel2);
            this.panel3.Controls.Add(this.baseLabel1);
            this.panel3.Size = new System.Drawing.Size(316, 172);
            // 
            // baseLabel1
            // 
            this.baseLabel1.AutoSize = true;
            this.baseLabel1.ForeColor = System.Drawing.Color.White;
            this.baseLabel1.Location = new System.Drawing.Point(10, 27);
            this.baseLabel1.Name = "baseLabel1";
            this.baseLabel1.Size = new System.Drawing.Size(41, 12);
            this.baseLabel1.TabIndex = 0;
            this.baseLabel1.Text = "工号：";
            // 
            // baseLabel2
            // 
            this.baseLabel2.AutoSize = true;
            this.baseLabel2.ForeColor = System.Drawing.Color.White;
            this.baseLabel2.Location = new System.Drawing.Point(10, 69);
            this.baseLabel2.Name = "baseLabel2";
            this.baseLabel2.Size = new System.Drawing.Size(41, 12);
            this.baseLabel2.TabIndex = 1;
            this.baseLabel2.Text = "密码：";
            // 
            // codeInput
            // 
            this.codeInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.codeInput.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.codeInput.Location = new System.Drawing.Point(57, 20);
            this.codeInput.Name = "codeInput";
            this.codeInput.Size = new System.Drawing.Size(241, 26);
            this.codeInput.TabIndex = 2;
            this.codeInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.codeInput_KeyPress);
            // 
            // pwdInput
            // 
            this.pwdInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pwdInput.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pwdInput.Location = new System.Drawing.Point(57, 62);
            this.pwdInput.Name = "pwdInput";
            this.pwdInput.PasswordChar = '*';
            this.pwdInput.Size = new System.Drawing.Size(241, 26);
            this.pwdInput.TabIndex = 3;
            this.pwdInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.pwdInput_KeyPress);
            // 
            // messageLabel
            // 
            this.messageLabel.AutoSize = true;
            this.messageLabel.ForeColor = System.Drawing.Color.White;
            this.messageLabel.Location = new System.Drawing.Point(55, 102);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new System.Drawing.Size(11, 12);
            this.messageLabel.TabIndex = 4;
            this.messageLabel.Text = "*";
            // 
            // baseButton2
            // 
            this.baseButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.baseButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton2.ForeColor = System.Drawing.Color.White;
            this.baseButton2.Location = new System.Drawing.Point(218, 127);
            this.baseButton2.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton2.Name = "baseButton2";
            this.baseButton2.Size = new System.Drawing.Size(80, 30);
            this.baseButton2.TabIndex = 15;
            this.baseButton2.Text = "Esc取消";
            this.baseButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton2.UseVisualStyleBackColor = false;
            this.baseButton2.Click += new System.EventHandler(this.baseButton2_Click);
            // 
            // baseButton1
            // 
            this.baseButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.baseButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton1.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton1.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton1.ForeColor = System.Drawing.Color.White;
            this.baseButton1.Location = new System.Drawing.Point(135, 127);
            this.baseButton1.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton1.Name = "baseButton1";
            this.baseButton1.Size = new System.Drawing.Size(80, 30);
            this.baseButton1.TabIndex = 14;
            this.baseButton1.Text = "Enter确认";
            this.baseButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton1.UseVisualStyleBackColor = false;
            this.baseButton1.Click += new System.EventHandler(this.baseButton1_Click);
            // 
            // AuthForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(322, 219);
            this.Name = "AuthForm";
            this.Text = "AuthForm";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Com.BaseLabel baseLabel2;
        private Com.BaseLabel baseLabel1;
        private Com.BaseTextInput pwdInput;
        private Com.BaseTextInput codeInput;
        private Com.BaseLabel messageLabel;
        private Com.BaseButton baseButton2;
        private Com.BaseButton baseButton1;
    }
}