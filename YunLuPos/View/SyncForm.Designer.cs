﻿namespace YunLuPos.View
{
    partial class SyncForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.baseButton2 = new YunLuPos.Com.BaseButton(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.syncWorker = new System.ComponentModel.BackgroundWorker();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Size = new System.Drawing.Size(421, 41);
            this.label1.Text = "数据同步";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Size = new System.Drawing.Size(421, 300);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Controls.Add(this.baseButton2);
            this.panel3.Size = new System.Drawing.Size(421, 259);
            // 
            // baseButton2
            // 
            this.baseButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.baseButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.baseButton2.CustomMouseDownColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.baseButton2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen;
            this.baseButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.baseButton2.ForeColor = System.Drawing.Color.White;
            this.baseButton2.Location = new System.Drawing.Point(332, 213);
            this.baseButton2.Margin = new System.Windows.Forms.Padding(0);
            this.baseButton2.Name = "baseButton2";
            this.baseButton2.Size = new System.Drawing.Size(80, 30);
            this.baseButton2.TabIndex = 17;
            this.baseButton2.Text = "Esc关闭";
            this.baseButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseButton2.UseVisualStyleBackColor = false;
            this.baseButton2.Click += new System.EventHandler(this.baseButton2_Click);
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox1.Location = new System.Drawing.Point(9, 15);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(403, 182);
            this.textBox1.TabIndex = 18;
            this.textBox1.Text = "数据同步";
            // 
            // syncWorker
            // 
            this.syncWorker.WorkerReportsProgress = true;
            this.syncWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.syncWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.syncWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // SyncForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 306);
            this.Name = "SyncForm";
            this.Text = "SyncForm";
            this.Load += new System.EventHandler(this.SyncForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Com.BaseButton baseButton2;
        private System.Windows.Forms.TextBox textBox1;
        private System.ComponentModel.BackgroundWorker syncWorker;
    }
}