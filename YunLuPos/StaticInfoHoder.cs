﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos
{
    public class StaticInfoHoder
    {
        /**
         * 基础信息
         * */
        public static CommInfo commInfo { get; set; }

        public static String version = "0.8";

        public static String FullScreen = "0";
        public static String ShowTop = "1";
        public static String Width = "1000";
        public static String Height = "700";
        public static String Style = "Blue";
        public static String PAY_SELECT_KEY = "F2";

        static StaticInfoHoder()
        {
            commInfo = new CommInfo();
            commInfo.cliqueCode = "0000";
            commInfo.shopCode = "1001";
            commInfo.shopName = "平顶山大德源超市长青路店";
            commInfo.posCode = "11";
            commInfo.posName = "测试机";
            commInfo.cashierCode = "200202";
            commInfo.cashierName = "李阳";
        }
       
    }
}
