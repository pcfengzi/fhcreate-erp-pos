﻿namespace YunLuPos.Com
{
    partial class PaymentPanel
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.baseLabel1 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel2 = new YunLuPos.Com.BaseLabel(this.components);
            this.baseLabel3 = new YunLuPos.Com.BaseLabel(this.components);
            this.payAmount = new YunLuPos.Com.BaseLabel(this.components);
            this.goodsCount = new YunLuPos.Com.BaseLabel(this.components);
            this.disAmount = new YunLuPos.Com.BaseLabel(this.components);
            this.SuspendLayout();
            // 
            // baseLabel1
            // 
            this.baseLabel1.AutoSize = true;
            this.baseLabel1.ForeColor = System.Drawing.Color.White;
            this.baseLabel1.Location = new System.Drawing.Point(5, 10);
            this.baseLabel1.Name = "baseLabel1";
            this.baseLabel1.Size = new System.Drawing.Size(35, 12);
            this.baseLabel1.TabIndex = 0;
            this.baseLabel1.Text = "金额:";
            // 
            // baseLabel2
            // 
            this.baseLabel2.AutoSize = true;
            this.baseLabel2.ForeColor = System.Drawing.Color.White;
            this.baseLabel2.Location = new System.Drawing.Point(5, 36);
            this.baseLabel2.Name = "baseLabel2";
            this.baseLabel2.Size = new System.Drawing.Size(35, 12);
            this.baseLabel2.TabIndex = 1;
            this.baseLabel2.Text = "数量:";
            // 
            // baseLabel3
            // 
            this.baseLabel3.AutoSize = true;
            this.baseLabel3.ForeColor = System.Drawing.Color.White;
            this.baseLabel3.Location = new System.Drawing.Point(5, 62);
            this.baseLabel3.Name = "baseLabel3";
            this.baseLabel3.Size = new System.Drawing.Size(35, 12);
            this.baseLabel3.TabIndex = 2;
            this.baseLabel3.Text = "优惠:";
            // 
            // payAmount
            // 
            this.payAmount.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.payAmount.ForeColor = System.Drawing.Color.Gold;
            this.payAmount.Location = new System.Drawing.Point(39, 5);
            this.payAmount.Name = "payAmount";
            this.payAmount.Size = new System.Drawing.Size(103, 23);
            this.payAmount.TabIndex = 3;
            this.payAmount.Text = "0.00";
            this.payAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // goodsCount
            // 
            this.goodsCount.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.goodsCount.ForeColor = System.Drawing.Color.White;
            this.goodsCount.Location = new System.Drawing.Point(39, 30);
            this.goodsCount.Name = "goodsCount";
            this.goodsCount.Size = new System.Drawing.Size(103, 23);
            this.goodsCount.TabIndex = 4;
            this.goodsCount.Text = "0.00";
            this.goodsCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // disAmount
            // 
            this.disAmount.Font = new System.Drawing.Font("Consolas", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.disAmount.ForeColor = System.Drawing.Color.White;
            this.disAmount.Location = new System.Drawing.Point(39, 56);
            this.disAmount.Name = "disAmount";
            this.disAmount.Size = new System.Drawing.Size(103, 23);
            this.disAmount.TabIndex = 5;
            this.disAmount.Text = "0.00";
            this.disAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PaymentPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Controls.Add(this.disAmount);
            this.Controls.Add(this.goodsCount);
            this.Controls.Add(this.payAmount);
            this.Controls.Add(this.baseLabel3);
            this.Controls.Add(this.baseLabel2);
            this.Controls.Add(this.baseLabel1);
            this.Name = "PaymentPanel";
            this.Size = new System.Drawing.Size(142, 88);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BaseLabel baseLabel1;
        private BaseLabel baseLabel2;
        private BaseLabel baseLabel3;
        private BaseLabel payAmount;
        private BaseLabel goodsCount;
        private BaseLabel disAmount;
    }
}
