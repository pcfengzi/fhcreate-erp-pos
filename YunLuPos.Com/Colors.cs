﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace YunLuPos.Com
{
   public class Colors
    {
        public static Color backColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));

        public static Color foreColor = Color.White;

        public static Color sharpColor = Color.Gold; //明显标记颜色

        public static Color errorColor = Color.Red; //错误颜色
    }
}
