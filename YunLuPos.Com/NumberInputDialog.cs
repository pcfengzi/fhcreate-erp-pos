﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YunLuPos.Com
{
    public partial class NumberInputDialog : BaseDialogForm
    {
        public NumberInputDialog()
        {
            InitializeComponent();
        }

        public NumberInputDialog(String caption)
        {
            InitializeComponent();
            this.label1.Text = caption;
        }

        private void baseTextInput1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // 如果输入的不是退格和数字，则屏蔽输入
            if (!(e.KeyChar == 8  || e.KeyChar == 46 || (e.KeyChar >= 48 && e.KeyChar <= 57)))
            {
                e.Handled = true;
            }
        }

        private void NumberInputDialog_Load(object sender, EventArgs e)
        {
            this.ActiveControl = this.number;
        }

        private void baseButton1_Click(object sender, EventArgs e)
        {
            confirm();
        }

        private void baseButton2_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void NumberInputDialog_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                confirm();
            }
        }

        void confirm()
        {
            try
            {
                Double.Parse(this.number.Text);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch
            {
                this.number.Text = "";
            }
        }
    }
}
