﻿using SQLiteSugar;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.DB.Service
{
    public class FileKeyBoardService
    {
        private static log4net.ILog logger = log4net.LogManager.GetLogger(typeof(KeyBoardService));
        private static String keyboardFile = Environment.CurrentDirectory + "/KEYBOARD";
        private static FileKeyBoardService inst = null;
        List<KeyBoard> keys = new List<KeyBoard>();
        public static FileKeyBoardService getService()
        {
            Console.WriteLine("--------------------key");
            if(inst == null)
            {
                inst = new FileKeyBoardService();
                try
                {
                    inst.init();
                }
                catch (Exception ex){
                    logger.Error("初始化键盘错误", ex);
                    return null;
                }
            }
            return inst;
        }

        void init()
        {
            Console.WriteLine("keyinit");
            if (File.Exists(keyboardFile))
            {
                Console.WriteLine("keyinit====");
                String[] lines = File.ReadAllLines(keyboardFile,Encoding.Default);
                foreach (String line in lines)
                {
                    if(line == null || "".Equals(line))
                    {
                        continue;
                    }
                    if (!line.StartsWith("#"))
                    {
                        String[] props = line.Split(',');
                        KeyBoard k = new KeyBoard();
                        k.keyCode = props[0];
                        k.commandKey = props[1];
                        k.commandName = props[2];
                        k.holder = props[3];
                        k.showTouch = props[4];
                        k.isEable = props[5];
                        k.seq = Int32.Parse(props[6]);
                        keys.Add(k);
                    }
                }
            }else
            {
                logger.Error("键盘配置文件缺失");
            }
        }

        private FileKeyBoardService()
        {
            
        }

        /**
         * 根据窗体获取快捷操作
         * */
        public List<KeyBoard> listByHolder(String holder)
        {
            if(holder == null)
            {
                return keys;
            }
            List<KeyBoard> result = new List<KeyBoard>();
            foreach(KeyBoard k in keys)
            {
                if (k.holder!= null && k.holder.Equals(holder))
                {
                    result.Add(k);
                }
            }
            return result;
        }


        /**
         * 根据窗体获取快
         * */
        public List<KeyBoard> listAll()
        {
            return keys;
        }


        /**
         * 根据窗体获取快捷操作
         * */
        public KeyBoard getByKeyCode(String holder,String keyCode)
        {
            if (keyCode == null)
            {
                return null;
            }
            foreach (KeyBoard k in keys)
            {
                if (k.holder != null && k.holder.Equals(holder) && k.keyCode != null && k.keyCode.Equals(keyCode))
                {
                    return k;
                }
            }
            return null;
        }


        /**
         * 根据窗体获取快捷操作
         * */
        public Boolean update(String commandKey, String keyCode)
        {
            KeyBoard key = null;
            foreach (KeyBoard k in keys)
            {
                if (k.commandKey != null && k.commandKey.Equals(commandKey))
                {
                    key = k;
                }
            }
            if (key == null)
            {
                return false;
            }
            key.keyCode = keyCode;
            FileStream fs = null;
            StringBuilder sb = new StringBuilder("#键值 命令 描述  所属窗体  显示快捷键  是否启用  排序号\r\n");
            foreach (KeyBoard k in keys)
            {
                StringBuilder line = new StringBuilder();
                line.Append(k.toString()+"\r\n");
                sb.Append(line.ToString());
            }
            try
            { 
                fs = File.Create(keyboardFile);
                byte[] bdata = Encoding.Default.GetBytes(sb.ToString());
                fs.Write(bdata, 0, bdata.Length);
                fs.Flush();
            }
            catch
            {
                return false;
            }
            finally
            {
                fs.Close();
            }
            return true;
        }

    }
}
