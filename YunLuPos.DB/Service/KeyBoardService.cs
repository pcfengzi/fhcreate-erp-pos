﻿using SQLiteSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.DB.Service
{
    public class KeyBoardService
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(KeyBoardService));
       
        /**
         * 根据窗体获取快捷操作
         * */
        public List<KeyBoard> listByHolder(String holder)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                   return db.Queryable<KeyBoard>()
                        .Where(it => it.holder == holder && it.showTouch == "1")
                        .OrderBy(it => it.seq,OrderByType.Asc)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }

        /**
        * 根据窗体获取快捷操作
        * */
        public List<KeyBoard> listAllByHolder(String holder)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    return db.Queryable<KeyBoard>()
                         .Where(it => it.holder == holder)
                         .OrderBy(it => it.seq, OrderByType.Asc)
                         .ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }


        /**
         * 根据窗体获取快
         * */
        public List<KeyBoard> listAll()
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    return db.Queryable<KeyBoard>()
                         .OrderBy(it => it.seq, OrderByType.Asc)
                         .ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }


        /**
         * 根据窗体获取快捷操作
         * */
        public KeyBoard getByKeyCode(String holder,String keyCode)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    return db.Queryable<KeyBoard>()
                         .Where(it =>
                             it.holder == holder &&
                             it.keyCode == keyCode )
                         .SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }


        /**
         * 根据窗体获取快捷操作
         * */
        public Boolean update(String commandKey, String keyCode)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    return db.Update<KeyBoard>(new
                    {
                        keyCode = keyCode
                        
                    }, it => it.commandKey == commandKey);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return false;
        }

    }
}
