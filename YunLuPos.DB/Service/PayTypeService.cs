﻿using SQLiteSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using YunLuPos.Entity;

namespace YunLuPos.DB.Service
{
    public class PayTypeService
    {
        private log4net.ILog logger = log4net.LogManager.GetLogger(typeof(PayTypeService));
        /**
         * 根据窗体获取快捷操作
         * */
        public List<PayType> list(Boolean hasDisable = false)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    if (hasDisable)
                    {
                        return db.Queryable<PayType>()
                            .OrderBy(it => it.seq, OrderByType.Asc)
                            .ToList();
                    }else
                    {
                        return db.Queryable<PayType>()
                            .Where(it => it.isEnable == "1" && it.isSubType == "0")
                            .OrderBy(it => it.seq, OrderByType.Asc)
                            .ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }

        /**
        * 获取退款方式
        * */
        public List<PayType> listRefundType()
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    return db.Queryable<PayType>()
                        .Where(it => it.isEnable == "1" && it.isSubType == "0" && it.enableReturn == "1")
                        .OrderBy(it => it.seq, OrderByType.Asc)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }

        /**
        * 批量新增或更新支付类型
        * */
        public Int64 insertOrUpdate(List<PayType> types)
        {
            if (types == null || types.Count <= 0)
            {
                return 0;
            }
            DBLocker.setLocker();
            SqlSugarClient db = null;
            try
            {
                using (db = SugarDao.GetInstance())
                {
                    db.BeginTran();
                    foreach (PayType t in types)
                    {
                        PayType dbType = db.Queryable<PayType>()
                            .Where(it => it.payTypeKey == t.payTypeKey)
                            .SingleOrDefault();
                        if (dbType != null)
                        {
                            db.Delete<PayType>(it => it.payTypeKey == t.payTypeKey);
                        }
                        if(t.openBox == null)
                        {
                            t.openBox = "1";
                        }
                        db.Insert<PayType>(t);
                    }
                    db.CommitTran();
                }
            }
            catch (Exception ex)
            {
                db.RollbackTran();
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return types.Count;
        }


        public String maxVersion()
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    Object obj = db.Queryable<PayType>().Max(it => it.ver);
                    if (obj == null || "".Equals(obj.ToString()))
                    {
                        return "0";
                    }
                    else
                    {
                        return obj.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return "0";
        }


        public PayType getTypeByKey(String payTypeKey)
        {
            DBLocker.setLocker();
            try
            {
                using (SqlSugarClient db = SugarDao.GetInstance())
                {
                    return db.Queryable<PayType>().Where(it => it.payTypeKey == payTypeKey).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            finally
            {
                DBLocker.release();
            }
            return null;
        }
    }
}
